<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/advertisement')->uses('Api\AdvertisementController@index');
Route::get('/advertisement/{advertisement}')->uses('Api\AdvertisementController@show');
Route::get('/category')->uses('Api\CategoryController@index');
Route::get('/category/{category}')->uses('Api\CategoryController@show');
Route::get('/medium')->uses('Api\MediumController@index');
Route::get('/medium/{medium}')->uses('Api\MediumController@show');
Route::post('/medium/type/ant-design/upload/image')->uses('Api\Medium\Type\AntDesign\Upload\ImageController@store');
Route::post('/medium/type/element-ui/upload/image')->uses('Api\Medium\Type\ElementUi\Upload\ImageController@store');
Route::post('/medium/type/tinymce')->uses('Api\Medium\Type\TinymceController@store');
Route::post('/medium/type/upload')->uses('Api\Medium\Type\UploadController@store');
Route::get('/menu')->uses('Api\MenuController@index');
Route::get('/menu/{menu}')->uses('Api\MenuController@show');
Route::get('/option')->uses('Api\OptionController@index');
Route::get('/option/{option}')->uses('Api\OptionController@show');
Route::get('/option/name/{name}')->uses('Api\Option\NameController@show');
Route::get('/post')->uses('Api\PostController@index');
Route::get('/post/{post}')->uses('Api\PostController@show');
Route::get('/prediction')->uses('Api\PredictionController@index');
Route::get('/prediction/{prediction}')->uses('Api\PredictionController@show');
Route::get('/tag')->uses('Api\TagController@index');
Route::post('/tag')->uses('Api\TagController@store');
