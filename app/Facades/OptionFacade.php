<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class OptionFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'option_facade';
    }
}