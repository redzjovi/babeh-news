<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MediumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'name' => $this->name,
            'file_extension' => $this->file_extension,
            'file_name' => $this->file_name,
            'file_path' => $this->file_path,
            'file_path_full' => $this->getFilePathUrlAttribute(),
            'file_type' => $this->file_type,
            'size' => $this->size,
            'youtube_url' => $this->youtube_url,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
