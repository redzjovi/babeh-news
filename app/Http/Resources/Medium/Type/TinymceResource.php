<?php

namespace App\Http\Resources\Medium\Type;

use Illuminate\Http\Resources\Json\JsonResource;

class TinymceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'location' => $this->getFilePathUrlAttribute(),
        ];
    }
}
