<?php

namespace App\Http\Controllers;

use App\Models\{
    HomeBottomAdvertisement,
    HomeHeaderPost,
    HomeMiddleAdvertisement,
    Menu,
    Option,
    Post,
    Prediction
};
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['bottomAdvertisements'] = HomeBottomAdvertisement::with('advertisement')->get();
        $data['headerPosts'] = HomeHeaderPost::with('post')->get();
        $data['menuNavbar'] = Menu::getMenuTreeWhereOptionName('body__navbar__menu_id');
        $data['middleAdvertisements'] = HomeMiddleAdvertisement::with('advertisement')->get();
        $data['prediction'] = Prediction::first();
        $data['storyPosts'] = Post::with('postAds')->latest()->limit(10)->get();

        return view('home/index', $data);
    }
}
