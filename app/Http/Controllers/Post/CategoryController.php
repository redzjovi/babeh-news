<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Models\Prediction;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @param Category $category
     * @return View
     */
    public function show(Category $category)
    {
        $data['category'] = $category;
        $data['posts'] = Post::whereHas('categories', function ($categories) use ($category) {
                $categories->where('category_id', $category->id);
            })
            ->latest()
            ->simplePaginate(20);
        $data['prediction'] = Prediction::first();
        
        return view('post/index', $data);
    }
}
