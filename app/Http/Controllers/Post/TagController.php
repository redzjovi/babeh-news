<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\{
    Post,
    Prediction,
    Tag
};
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * @param Tag $tag
     * @return View
     */
    public function show(Tag $tag)
    {
        $data['tag'] = $tag;
        $data['posts'] = Post::whereHas('tags', function ($tags) use ($tag) {
                $tags->where('tag_id', $tag->id);
            })
            ->latest()
            ->simplePaginate(20);
        $data['prediction'] = Prediction::first();
        
        return view('post/index', $data);
    }
}
