<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Advertisement\StoreRequest;
use App\Models\{
    Advertisement,
    Tag
};
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['advertisements'] = Advertisement::latest()->get();
        return view('backend/advertisement/index', $data);
    }

    /**
     * @return View
     */
    public function create()
    {
        $data['model'] = new Advertisement;
        return view('backend/advertisement/create', $data);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $model = new Advertisement;
        $model->fill($request->input())->save();
        $model->images()->sync($request->input('image'));
        $model->tagsSyncByTagNames($request->input('tag.name'));

        session()->flash('alert-success', 'Stored.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Advertisement $advertisement
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {
        $data['model'] = $advertisement;
        return view('backend/advertisement/edit', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Advertisement $advertisement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreRequest $request, Advertisement $advertisement)
    {
        $model = $advertisement;
        $model->fill($request->input())->save();
        $model->images()->sync($request->input('image'));
        $model->tagsSyncByTagNames($request->input('tag.name'));

        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Advertisement $advertisement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Advertisement $advertisement)
    {
        $advertisement->delete();
        session()->flash('alert-success', 'Deleted.');
        return redirect()->back();
    }
}
