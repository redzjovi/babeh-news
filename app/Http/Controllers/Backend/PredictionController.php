<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Prediction\StoreRequest;
use App\Models\Prediction;
use Illuminate\Http\Request;

class PredictionController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['predictions'] = Prediction::latest()->get();
        return view('backend/prediction/index', $data);
    }

    /**
     * @param Prediction $prediction
     * @return \Illuminate\Http\Response
     */
    public function edit(Prediction $prediction)
    {
        $data['model'] = $prediction;
        return view('backend/prediction/edit', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Prediction $prediction
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreRequest $request, Prediction $prediction)
    {
        $model = $prediction;
        $model->fill($request->input())->save();
        
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }
}
