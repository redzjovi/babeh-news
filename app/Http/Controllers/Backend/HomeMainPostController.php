<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\HomeMainPost\UpdateRequest;
use App\Models\HomeMainPost;
use Illuminate\Http\Request;

class HomeMainPostController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['homeMainPost'] = HomeMainPost::all();
        return view('backend/home_main_post/index', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        HomeMainPost::query()->delete();
        if ($postIds = $request->input('post_id')) {
            foreach ($postIds as $postId) {
                HomeMainPost::create(['post_id' => $postId]);
            }
        }
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }
}
