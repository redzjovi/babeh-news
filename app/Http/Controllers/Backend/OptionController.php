<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Option\StoreRequest;
use App\Models\Option;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['options'] = Option::latest()->get();
        return view('backend/option/index', $data);
    }

    /**
     * @return View
     */
    public function create()
    {
        $data['model'] = new Option;
        return view('backend/option/create', $data);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $model = new Option;
        $model->fill($request->input())->save();

        session()->flash('alert-success', 'Stored.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Option $option
     * @return \Illuminate\Http\Response
     */
    public function edit(Option $option)
    {
        $data['model'] = $option;
        return view('backend/option/edit', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Option $option
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreRequest $request, Option $option)
    {
        $model = $option;
        $model->fill($request->input())->save();

        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Option $option
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Option $option)
    {
        $option->delete();
        session()->flash('alert-success', 'Deleted.');
        return redirect()->back();
    }
}
