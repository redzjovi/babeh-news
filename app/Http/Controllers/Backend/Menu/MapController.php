<?php

namespace App\Http\Controllers\Backend\Menu;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Menu\UpdateRequest;
use App\Models\Menu;
use Illuminate\Http\Request;

class MapController extends Controller
{
    /**
     * @param Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $data['model'] = $menu;
        return view('backend/menu/map/edit', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Menu $menu)
    {
        $model = $menu;
        $model->fill($request->input())->save();
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }

    /**
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Menu $menu)
    {
        $menu->delete();
        session()->flash('alert-success', 'Deleted.');
        return redirect()->route('backend.menu.index');
    }
}
