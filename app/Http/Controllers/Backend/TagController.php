<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Tag\StoreRequest;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['tags'] = Tag::latest()->get();
        return view('backend/tag/index', $data);
    }

    /**
     * @return View
     */
    public function create()
    {
        $data['model'] = new Tag;
        return view('backend/tag/create', $data);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $model = new Tag;
        $model->fill($request->input())->save();
        session()->flash('alert-success', 'Stored.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $data['model'] = $tag;
        return view('backend/tag/edit', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Tag $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreRequest $request, Tag $tag)
    {
        $model = $tag;
        $model->fill($request->input())->save();
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Tag $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Tag $tag)
    {
        $tag->delete();
        session()->flash('alert-success', 'Deleted.');
        return redirect()->back();
    }
}
