<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\{
    Medium,
    Option
};
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['body__navbar_brand__type'] = Option::firstOrNew(['name' => 'body__navbar_brand__type']);
        $data['body__navbar__menu_id'] = Option::firstOrNew(['name' => 'body__navbar__menu_id']);
        $data['html__head__title'] = Option::firstOrNew(['name' => 'html__head__title']);
        $data['site__image_id'] = Option::firstOrNew(['name' => 'site__image_id']);
        $data['site__image_src'] = Option::firstOrNew(['name' => 'site__image_src']);
        $data['site__name'] = Option::firstOrNew(['name' => 'site__name']);

        return view('backend/setting/index', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        if ($request->has('body__navbar_brand__type')) {
            $body__navbar_brand__type = Option::firstOrNew(['name' => 'body__navbar_brand__type']);
            $body__navbar_brand__type->value = $request->input('body__navbar_brand__type');
            $body__navbar_brand__type->save();
        }

        if ($request->has('body__navbar__menu_id')) {
            $body__navbar__menu_id = Option::firstOrNew(['name' => 'body__navbar__menu_id']);
            $body__navbar__menu_id->value = $request->input('body__navbar__menu_id');
            $body__navbar__menu_id->save();
        }

        if ($request->has('html__head__title')) {
            $html__head__title = Option::firstOrNew(['name' => 'html__head__title']);
            $html__head__title->value = $request->input('html__head__title');
            $html__head__title->save();
        }

        $site__image_id = Option::firstOrNew(['name' => 'site__image_id']);
        $site__image_id->value = '';
        $site__image_src = Option::firstOrNew(['name' => 'site__image_src']);
        $site__image_src->value = '';
        
        if ($request->has('site__image_id')) {
            foreach ($request->input('site__image_id') as $mediumId) {
                $medium = Medium::find($mediumId);
                $site__image_id->value = $medium->id;
                $site__image_src->value = $medium->getFilePathUrlAttribute();
            }
        }
        $site__image_id->save();
        $site__image_src->save();

        if ($request->has('site__name')) {
            $site__name = Option::firstOrNew(['name' => 'site__name']);
            $site__name->value = $request->input('site__name');
            $site__name->save();
        }

        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }
}
