<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\HomeBottomAdvertisement\UpdateRequest;
use App\Models\HomeBottomAdvertisement;
use Illuminate\Http\Request;

class HomeBottomAdvertisementController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['homeBottomAdvertisement'] = HomeBottomAdvertisement::with('advertisement')->get();
        return view('backend/home_bottom_advertisement/index', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        HomeBottomAdvertisement::query()->delete();
        if ($advertisementIds = $request->input('advertisement_id')) {
            foreach ($advertisementIds as $advertisementId) {
                HomeBottomAdvertisement::create(['advertisement_id' => $advertisementId]);
            }
        }
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }
}
