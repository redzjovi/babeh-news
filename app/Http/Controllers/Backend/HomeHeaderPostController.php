<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\HomeHeaderPost\UpdateRequest;
use App\Models\HomeHeaderPost;
use Illuminate\Http\Request;

class HomeHeaderPostController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['homeHeaderPost'] = HomeHeaderPost::with('post')->get();
        return view('backend/home_header_post/index', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        HomeHeaderPost::query()->delete();
        if ($postIds = $request->input('post_id')) {
            foreach ($postIds as $postId) {
                HomeHeaderPost::create(['post_id' => $postId]);
            }
        }
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }
}
