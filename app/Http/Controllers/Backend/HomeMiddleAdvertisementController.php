<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\HomeMiddleAdvertisement\UpdateRequest;
use App\Models\HomeMiddleAdvertisement;
use Illuminate\Http\Request;

class HomeMiddleAdvertisementController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['homeMiddleAdvertisement'] = HomeMiddleAdvertisement::with('advertisement')->get();
        return view('backend/home_middle_advertisement/index', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        HomeMiddleAdvertisement::query()->delete();
        if ($advertisementIds = $request->input('advertisement_id')) {
            foreach ($advertisementIds as $advertisementId) {
                HomeMiddleAdvertisement::create(['advertisement_id' => $advertisementId]);
            }
        }
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }
}
