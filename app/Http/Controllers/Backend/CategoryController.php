<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Category\StoreRequest;
use App\Http\Requests\Api\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['categories'] = Category::latest()->get();
        return view('backend/category/index', $data);
    }

    /**
     * @return View
     */
    public function create()
    {
        $data['model'] = new Category;
        $data['parents'] = Category::getCategoriesAsTree();
        return view('backend/category/create', $data);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $model = new Category;
        $model->fill($request->input())->save();
        session()->flash('alert-success', 'Stored.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $data['model'] = $category;
        $data['parents'] = Category::getCategoriesAsTree($category->id);
        return view('backend/category/edit', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Category $category)
    {
        $model = $category;
        $model->fill($request->input())->save();
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Category $category)
    {
        $category->delete();
        session()->flash('alert-success', 'Deleted.');
        return redirect()->back();
    }
}
