<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Post\StoreRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\PostAds;
use App\Models\Tag;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['posts'] = Post::latest()->get();
        return view('backend/post/index', $data);
    }

    /**
     * @return View
     */
    public function create()
    {
        $data['categories'] = Category::getCategoriesWithTreePrefix('&nbsp;&nbsp;&nbsp;&nbsp;');
        $data['model'] = new Post;
        return view('backend/post/create', $data);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $model = new Post;
        $model->fill($request->input())->save();

        if ($postAdsLink = $request->input('post_ads.link')) {
            PostAds::createOrUpdatePostAdsSetLinkWherePost($postAdsLink, $model);
        }

        $model->categories()->sync($request->input('category_id'));
        $model->images()->sync($request->input('image'));
        $model->tagsSyncByTagNames($request->input('tag.name'));

        session()->flash('alert-success', 'Stored.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $data['categories'] = Category::getCategoriesWithTreePrefix('&nbsp;&nbsp;&nbsp;&nbsp;');
        $data['model'] = $post;
        return view('backend/post/edit', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreRequest $request, Post $post)
    {
        $model = $post;
        $model->fill($request->input())->save();

        if ($postAdsLink = $request->input('post_ads.link')) {
            PostAds::createOrUpdatePostAdsSetLinkWherePost($postAdsLink, $model);
        }

        $model->categories()->sync($request->input('category_id'));
        $model->images()->sync($request->input('image'));
        $model->tagsSyncByTagNames($request->input('tag.name'));

        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Post $post)
    {
        $post->delete();
        session()->flash('alert-success', 'Deleted.');
        return redirect()->back();
    }
}
