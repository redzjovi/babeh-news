<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Menu\StoreRequest;
use App\Http\Requests\Api\Menu\UpdateRequest;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['menus'] = Menu::latest()->get();
        return view('backend/menu/index', $data);
    }

    /**
     * @return View
     */
    public function create()
    {
        $data['model'] = new Menu;
        return view('backend/menu/create', $data);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $model = new Menu;
        $model->fill($request->input())->save();
        session()->flash('alert-success', 'Stored.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $data['model'] = $menu;
        return view('backend/menu/edit', $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Menu $menu)
    {
        $model = $menu;
        $model->fill($request->input())->save();
        session()->flash('alert-success', 'Updated.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Menu $menu)
    {
        $menu->delete();
        session()->flash('alert-success', 'Deleted.');
        return redirect()->back();
    }
}
