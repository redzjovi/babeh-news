<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OptionResource;
use App\Models\Option;
use DB;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Option::query();

        if ($id = $request->query('id')) {
            $query = $query->where('id', $id);
        }

        if ($name = $request->query('name')) {
            $query = $query->where('name', 'like', '%'.$name.'%');
        }

        if ($orderBy = $request->query('order_by')) {
            if (
                in_array($orderBy, [
                    'id',
                    'name',
                    'created_at',
                    'updated_at',
                ])
            ) {
                $query = $query->orderBy($orderBy);
            } elseif (
                in_array($orderBy, [
                    '-id',
                    '-name',
                    '-created_at',
                    '-updated_at',
                ])
            ) {
                $query = $query->orderBy(
                    str_replace('-', '', $orderBy),
                    'desc'
                );
            }
        }

        $options = $query->paginate();

        return OptionResource::collection($options);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param Option $option
     * @return \Illuminate\Http\Response
     */
    public function show(Option $option)
    {
        return new OptionResource($option);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
