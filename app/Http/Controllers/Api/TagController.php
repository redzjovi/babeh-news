<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Tag\StoreRequest;
use App\Http\Resources\TagResource;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Tag::query();

        if ($id = $request->query('id')) {
            $query = $query->where('id', $id);
        }

        if ($name = $request->query('name')) {
            $query = $query->where('name', 'like', '%'.$name.'%');
        }

        if ($slug = $request->query('slug')) {
            $query = $query->where('slug', $slug);
        }

        if ($excerpt = $request->query('excerpt')) {
            $query = $query->where('excerpt', 'like', '%'.$excerpt.'%');
        }

        if ($orderBy = $request->query('order_by')) {
            if (
                in_array($orderBy, [
                    'id',
                    'name',
                    'slug',
                    'excerpt',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ])
            ) {
                $query = $query->orderBy($orderBy);
            } elseif (
                in_array($orderBy, [
                    '-id',
                    '-name',
                    '-slug',
                    '-excerpt',
                    '-created_at',
                    '-updated_at',
                    '-deleted_at',
                ])
            ) {
                $query = $query->orderBy(
                    str_replace('-', '', $orderBy),
                    'desc'
                );
            }
        }
        
        $tags = $query->paginate();

        return TagResource::collection($tags);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $tag = new Tag;
        $tag->fill($request->input())->save();
        return new TagResource($tag);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
