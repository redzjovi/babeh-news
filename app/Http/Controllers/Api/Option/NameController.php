<?php

namespace App\Http\Controllers\Api\Option;

use App\Http\Controllers\Controller;
use App\Http\Resources\OptionResource;
use App\Models\Option;
use Illuminate\Http\Request;

class NameController extends Controller
{
    /**
     * @param string $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $name)
    {
        $option = Option::getOptionOrFailWhereName($name);
        return new OptionResource($option);
    }
}
