<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Menu::query();

        if ($id = $request->query('id')) {
            $query = $query->where('id', $id);
        }

        if ($name = $request->query('name')) {
            $query = $query->where('name', 'like', '%'.$name.'%');
        }

        if ($slug = $request->query('slug')) {
            $query = $query->where('slug', $slug);
        }

        if ($excerpt = $request->query('excerpt')) {
            $query = $query->where('excerpt', 'like', '%'.$excerpt.'%');
        }

        if ($orderBy = $request->query('order_by')) {
            if (
                in_array($orderBy, [
                    'id',
                    'name',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ])
            ) {
                $query = $query->orderBy($orderBy);
            } elseif (
                in_array($orderBy, [
                    '-id',
                    '-name',
                    '-created_at',
                    '-updated_at',
                    '-deleted_at',
                ])
            ) {
                $query = $query->orderBy(
                    str_replace('-', '', $orderBy),
                    'desc'
                );
            }
        }

        $menus = $query->paginate();

        return MenuResource::collection($menus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        return new MenuResource($menu);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
