<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;
use DB;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Post::query();

        if ($id = $request->query('id')) {
            $query = $query->where('id', $id);
        }

        if ($ids = $request->query('ids')) {
            $query = $query->whereIn('id', explode(',', $ids));
            $query = $query->orderByRaw(DB::raw('FIELD(id, '.$ids.')'));
        }

        if ($title = $request->query('title')) {
            $query = $query->where('title', 'like', '%'.$title.'%');
        }

        if ($slug = $request->query('slug')) {
            $query = $query->where('slug', $slug);
        }

        if ($excerpt = $request->query('excerpt')) {
            $query = $query->where('excerpt', 'like', '%'.$excerpt.'%');
        }

        if ($view_count = $request->query('view_count')) {
            $query = $query->where('view_count', 'like', '%'.$view_count.'%');
        }

        if ($orderBy = $request->query('order_by')) {
            if (
                in_array($orderBy, [
                    'id',
                    'title',
                    'slug',
                    'excerpt',
                    'view_count',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ])
            ) {
                $query = $query->orderBy($orderBy);
            } elseif (
                in_array($orderBy, [
                    '-id',
                    '-title',
                    '-slug',
                    '-excerpt',
                    '-view_count',
                    '-created_at',
                    '-updated_at',
                    '-deleted_at',
                ])
            ) {
                $query = $query->orderBy(
                    str_replace('-', '', $orderBy),
                    'desc'
                );
            }
        }
        
        $posts = $query->paginate();

        // dump($query->toSql());

        return PostResource::collection($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return new PostResource($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
