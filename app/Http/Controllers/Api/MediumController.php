<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MediumResource;
use App\Models\Medium;
use Illuminate\Http\Request;

class MediumController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Medium::query();

        if ($id = $request->query('id')) {
            $query = $query->where('id', $id);
        }

        if ($type = $request->query('type')) {
            $query = $query->where('type', $type);
        }

        if ($name = $request->query('name')) {
            $query = $query->where('name', 'like', '%'.$name.'%');
        }

        if ($file_extension = $request->query('file_extension')) {
            $query = $query->where('file_extension', $file_extension);
        }
        
        if ($file_name = $request->query('file_name')) {
            $query = $query->where('file_name', 'like', '%'.$file_name.'%');
        }

        if ($file_path = $request->query('file_path')) {
            $query = $query->where('file_path', 'like', '%'.$file_path.'%');
        }

        if ($file_type = $request->query('file_type')) {
            $query = $query->where('file_type', $file_type);
        }

        if ($size = $request->query('size')) {
            $query = $query->where('size', 'like', '%'.$size.'%');
        }

        if ($youtube_url = $request->query('youtube_url')) {
            $query = $query->where('youtube_url', 'like', '%'.$youtube_url.'%');
        }

        if ($orderBy = $request->query('order_by')) {
            if (
                in_array($orderBy, [
                    'id',
                    'type',
                    'name',
                    'file_extension',
                    'file_name',
                    'file_path',
                    'file_type',
                    'size',
                    'youtube_url',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ])
            ) {
                $query = $query->orderBy($orderBy);
            } elseif (
                in_array($orderBy, [
                    '-id',
                    '-type',
                    '-name',
                    '-file_extension',
                    '-file_name',
                    '-file_path',
                    '-file_type',
                    '-size',
                    '-youtube_url',
                    '-created_at',
                    '-updated_at',
                    '-deleted_at',
                ])
            ) {
                $query = $query->orderBy(
                    str_replace('-', '', $orderBy),
                    'desc'
                );
            }
        }
        
        $media = $query->paginate();

        return MediumResource::collection($media);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param Medium $medium
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Medium $medium)
    {
        return new MediumResource($medium);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
