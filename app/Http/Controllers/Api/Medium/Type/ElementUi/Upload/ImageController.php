<?php

namespace App\Http\Controllers\Api\Medium\Type\ElementUi\Upload;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Medium\Type\ElementUi\Upload\Image\StoreRequest;
use App\Models\Medium;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $file = $request->file('file');
        $medium = Medium::createMediumFromTypeAndFile(Medium::TYPE__ELEMENT_UI_UPLOAD_IMAGE, $file);

        return response()->json([
            'id' => $medium->id,
            'name' => $medium->file_name,
            'url' => $medium->getFilePathUrlAttribute(),
        ]);
    }
}
