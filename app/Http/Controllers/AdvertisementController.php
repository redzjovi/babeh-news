<?php

namespace App\Http\Controllers;

use App\Models\Advertisement;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    /**
     * @param Advertisement $advertisement
     * @return View
     */
    public function show(Advertisement $advertisement)
    {
        $data['advertisement'] = $advertisement;
        return view('advertisement/show', $data);
    }
}
