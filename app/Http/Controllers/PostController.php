<?php

namespace App\Http\Controllers;

use App\Models\{
    Post,
    Prediction
};
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $data['posts'] = Post::latest()->simplePaginate(20);
        $data['prediction'] = Prediction::first();
        
        return view('post/index', $data);
    }

    /**
     * @param Post $post
     * @return View
     */
    public function show(Post $post)
    {
        $post->increment('view_count');
        $tagIds = $post->tags->pluck('id')->all();

        $data['post'] = $post;
        $data['relatedPosts'] = Post::getPostsWhereIdIsNotAndTagIdInLimit4Latest($post->id, $tagIds);

        return view('post/show', $data);
    }
}
