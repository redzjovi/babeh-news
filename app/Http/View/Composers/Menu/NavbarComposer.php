<?php

namespace App\Http\View\Composers\Menu;

use App\Models\Menu;
use Illuminate\View\View;

class NavbarComposer
{
    /**
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $data['menuNavbar'] = Menu::getMenuTreeWhereOptionName('body__navbar__menu_id');

        $view->with($data);
    }
}