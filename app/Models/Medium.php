<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medium extends Model
{
    const TYPE__ELEMENT_UI_UPLOAD_IMAGE = 'element_ui_upload_image';
    const TYPE__TINYMCE = 'tinymce';
    const TYPE__UPLOAD = 'upload';

    use SoftDeletes;

    protected $fillable = [
        'type',
        'name',
        'file_extension',
        'file_name',
        'file_path',
        'file_type',
        'size',
        'youtube_url',
    ];

    protected $table = 'medium';

    /**
     * @return string
     */
    public function getFilePathUrlAttribute()
    {
        return \Storage::disk('public')->url($this->file_path);
    }

    /**
     * @return array $elementUiUploadFileList
     */
    public function getImagesToElementUiUploadFileList()
    {
        $elementUiUploadFileList[] = [
            'name' => $this->name,
            'percentage' => 100,
            'raw' => 'file',
            'response' => [
                'name' => $this->name,
                'id' => $this->id,
                'url' => $this->getFilePathUrlAttribute(),
            ],
            'size' => $this->size,
            'status' => 'success',
            'uid' => uniqid(),
            'url' => $this->getFilePathUrlAttribute(),
        ];
        
        return $elementUiUploadFileList;
    }

    /**
     * @param string $type
     * @param \Illuminate\Http\UploadedFile $file
     * @return \App\Models\Medium $medium
     */
    public static function createMediumFromTypeAndFile($type, $file)
    {
        $medium = new Medium();
        $medium->type = $type;
        $medium->name = $file->getClientOriginalName();
        $medium->file_extension = $file->getClientOriginalExtension();
        $medium->file_name = $file->getClientOriginalName();
        $medium->file_type = $file->getMimeType();
        $medium->size = $file->getSize();
        $medium->save();

        $medium->file_path = $file->storeAs('medium/'.$medium->id, $medium->name, 'public');
        $medium->save();

        return $medium;
    }
}
