<?php

namespace App\Models;

use App\Libraries\MenuLibrary;
use App\Models\Option;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'value',
    ];

    protected $table = 'menu';

    /**
     * @return array
     */
    public function getMenuTree()
    {
        $value = $this->value ? json_decode($this->value, true) : [];
        $menuLibrary = new MenuLibrary($value);
        $listArray = $menuLibrary->getListArray();
        return $menuLibrary->mapListArrayToList($listArray);
    }

    /**
     * @param integer $id
     * @return Menu
     */
    public static function getMenuWhereId(int $id)
    {
        return self::find($id);
    }

    /**
     * @param string $optionName
     * @return array|null $menuTree
     */
    public static function getMenuTreeWhereOptionName(string $optionName)
    {
        $option = Option::getOptionWhereName($optionName);
        if (! $option) {
            return null;
        }

        $menu = Menu::getMenuWhereId($option->value);
        if (! $menu) {
            return null;
        }

        $menuTree = $menu->getMenuTree();
        
        return $menuTree;
    }
}
