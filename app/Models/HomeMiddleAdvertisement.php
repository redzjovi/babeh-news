<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeMiddleAdvertisement extends Model
{
    protected $fillable = [
        'advertisement_id',
    ];

    protected $table = 'home_middle_advertisement';

    public $incrementing = false;

    public $timestamps = false;

    public function advertisement()
    {
        return $this->belongsTo(Advertisement::class)->withTrashed();
    }
}
