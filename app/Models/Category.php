<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use redzjovi\php\ArrayHelper;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'excerpt',
        'content',
        'parent_id',
    ];

    protected $table = 'category';

    /**
     * @param string $value
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    /**
     * @return array $categories
     */
    public static function getCategoriesAsTree()
    {
        $categories = self::orderBy('name')->get()->toArray();
        $categories = ArrayHelper::copyKeyName($categories, 'parent_id', 'parent');
        $categories = ArrayHelper::buildTree($categories);
        $categories = collect($categories)->where('parent', '=', '');
        
        return $categories;
    }

    /**
     * @return array $categories
     */
    public static function getCategoriesWithTreePrefix($treePrefix = '-')
    {
        $categories = self::orderBy('name')->get()->toArray();
        $categories = ArrayHelper::copyKeyName($categories, 'parent_id', 'parent');
        $categories = ArrayHelper::buildTree($categories);
        $categories = ArrayHelper::printTree($categories, $treePrefix);
        
        return $categories;
    }
}
