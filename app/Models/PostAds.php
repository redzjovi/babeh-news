<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostAds extends Model
{
    protected $fillable = [
        'post_id',
        'link',
    ];

    protected $table = 'post_ads';

    public function post()
    {
        return $this->belongsTo(Post::class)->withTrashed();
    }

    /**
     * @param string $link
     * @param Post $post
     * @return null|PostAds
     */
    public static function createOrUpdatePostAdsSetLinkWherePost($link, $post)
    {
        $postAds = $post->postAds ?: new PostAds;
        $postAds->link = $link;
        $post->postAds()->save($postAds);
        return $post->postAds;
    }
}
