<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeHeaderPost extends Model
{
    protected $fillable = [
        'post_id',
    ];

    protected $table = 'home_header_post';

    public $incrementing = false;

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class)->withTrashed();
    }

    /**
     * @return Post
     */
    public static function getHomeHeaderPostPostOrPost()
    {
        if ($model = self::first()) {
            return $model->post;
        }

        return Post::latest()->first();
    }
}
