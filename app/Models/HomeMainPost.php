<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeMainPost extends Model
{
    protected $fillable = [
        'post_id',
    ];

    protected $table = 'home_main_post';

    public $incrementing = false;

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class)->withTrashed();
    }

    /**
     * @return null|Post[] $posts
     */
    public static function getHomeMainPostPosts()
    {
        $posts = [];

        if ($homeMainPosts = self::with(['post', 'post.postAds'])->get()) {
            foreach ($homeMainPosts as $homeMainPost) {
                $posts[] = $homeMainPost->post;
            }
        }

        return $posts;
    }
}
