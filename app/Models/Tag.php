<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Tag extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'excerpt',
        'content',
    ];

    protected $table = 'tag';

    /**
     * @param string $value
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    /**
     * @param array $names
     * @return object[]
     */
    public static function createNames($names)
    {
        $tags = [];

        foreach ($names as $name) {
            $tags[] = self::firstOrCreate(['name' => $name]);
        }

        return collect($tags);
    }
}
