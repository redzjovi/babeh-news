<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'name',
        'value',
    ];

    protected $table = 'option';

    public function medium()
    {
        return $this->belongsTo(Medium::class, 'value', 'id');
    }

    /**
     * @return Option[]
     */
    public static function createOptionsDefault()
    {
        $options = [];
        $optionsDefault = [
            [
                'name' => 'body__navbar__menu_id',
                'value' => '', // menu_id
            ],
            [
                'name' => 'body__navbar_brand__type',
                'value' => 'text', // image|text
            ],
            [
                'name' => 'html__head__title',
                'value' => 'babeh-news.com',
            ],
            [
                'name' => 'site__image_src',
                'value' => 'https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg',
            ],
            [
                'name' => 'site__name',
                'value' => 'Babeh News',
            ],
        ];

        foreach ($optionsDefault as $optionDefault) {
            $option = self::where('name', $optionDefault['name'])->first();
            
            if ($option) {
                $options[] = $option;
            } else {
                $options[] = self::create($optionDefault);
            }
        }

        return $options;
    }
    
    /**
     * @param string $name
     * @return null|Option
     */
    public static function getOptionOrFailWhereName($name)
    {
        return self::where('name', $name)->firstOrFail();
    }
    
    /**
     * @param string $name
     * @return null|Option
     */
    public static function getOptionWhereName($name)
    {
        return self::where('name', $name)->first();
    }
}
