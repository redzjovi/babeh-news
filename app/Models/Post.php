<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'excerpt',
        'content',
    ];

    protected $table = 'post';

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categorizable', 'categorizable');
    }

    /**
     * @return array $elementUiUploadFileList
     */
    public function getImagesToElementUiUploadFileList()
    {
        $elementUiUploadFileList = collect($this->images)->map(function ($image) {
            return [
                'name' => $image->name,
                'percentage' => 100,
                'raw' => 'file',
                'response' => [
                    'name' => $image->name,
                    'id' => $image->id,
                    'url' => $image->getFilePathUrlAttribute(),
                ],
                'size' => $image->size,
                'status' => 'success',
                'uid' => uniqid(),
                'url' => $image->getFilePathUrlAttribute(),
            ];
        });

        return $elementUiUploadFileList;
    }

    public function getImage()
    {
        return $this->images->first();
    }

    public function images()
    {
        return $this->morphToMany(Medium::class, 'mediumable', 'mediumable');
    }

    public function postAds()
    {
        return $this->hasOne(PostAds::class);
    }

    /**
     * @param string $value
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable', 'taggable');
    }

        /**
     * @param array $tagNames
     */
    public function tagsSyncByTagNames($tagNames = [])
    {
        $tagIds = [];
        if (is_array($tagNames)) {
            $tags = Tag::createNames($tagNames);
            $tagIds = $tags->pluck('id')->all();
        }
        $this->tags()->sync($tagIds);
    }

    /**
     * @return object[]
     */
    public static function getPostsWhereCreatedAtThisWeekOrderByViewCountDescLimit5()
    {
        return self::query()
            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->orderBy('view_count', 'desc')
            ->limit(5)
            ->get();
    }

    /**
     * @param int $id
     * @param array $tagIds
     * @return object[]
     */
    public static function getPostsWhereIdIsNotAndTagIdInLimit4Latest($id, $tagIds)
    {
        return self::query()
            ->where('id', '!=', $id)
            ->whereHas('tags', function ($tagsBuilder) use ($tagIds) {
                $tagsBuilder->whereIn('tag_id', $tagIds);
            })
            ->limit(4)
            ->get();
    }
}
