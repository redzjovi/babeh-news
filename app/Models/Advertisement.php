<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Advertisement extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'excerpt',
        'content',
    ];

    protected $table = 'advertisement';

    /**
     * @return array $elementUiUploadFileList
     */
    public function getImagesToElementUiUploadFileList()
    {
        $elementUiUploadFileList = collect($this->images)->map(function ($image) {
            return [
                'name' => $image->name,
                'percentage' => 100,
                'raw' => 'file',
                'response' => [
                    'name' => $image->name,
                    'id' => $image->id,
                    'url' => $image->getFilePathUrlAttribute(),
                ],
                'size' => $image->size,
                'status' => 'success',
                'uid' => uniqid(),
                'url' => $image->getFilePathUrlAttribute(),
            ];
        });

        return $elementUiUploadFileList;
    }

    public function getImage()
    {
        return $this->images->first();
    }

    public function images()
    {
        return $this->morphToMany(Medium::class, 'mediumable', 'mediumable');
    }

    /**
     * @param string $value
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable', 'taggable');
    }

    /**
     * @param array $tagNames
     */
    public function tagsSyncByTagNames($tagNames = [])
    {
        $tagIds = [];
        if (is_array($tagNames)) {
            $tags = Tag::createNames($tagNames);
            $tagIds = $tags->pluck('id')->all();
        }
        $this->tags()->sync($tagIds);
    }
}
