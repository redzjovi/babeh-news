<?php

namespace App\Repositories;

use App\Models\Option;
use Cache;

class OptionRepository
{
    public $model;

    public function __construct()
    {
        $this->model = new Option;
    }

    /**
     * @param string $name
     * @return string
     */
    public function get($name)
    {
        $cacheName = 'option:name:'.$name;

        if (Cache::has($cacheName)) {
            return Cache::get($cacheName);
        }

        if ($option = Option::getOptionWhereName($name)) {
            Cache::put($cacheName, $option->value, now()->addMinutes(60));
            return $option->value;
        }

        return '';
    }
}