<?php

namespace App\Libraries;

use App\Libraries\Menu\ElLibrary;

class MenuLibrary
{
    private $list = [];
    private $list_array = [];

    /**
     * @param array $listArray
     */
    public function __construct($listArray)
    {
        $this->setListArray($listArray);
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }
    
    /**
     * @return array
     */
    public function getListArray()
    {
        return $this->list_array;
    }
    
    /**
     * @return array $listArray
     */
    public function mapListArrayToList(array $listArray)
    {
        $list = [];
        
        foreach ($listArray as $i => $el) {
            $el = new ElLibrary($el);
            $list[$i] = $el->getAttributes();
        }

        return $list;
    }

    /**
     * @param array $value
     * @return $this
     */
    public function setList($value)
    {
        $this->list = $value;
        return $this;
    }

    /**
     * @param array $value
     * @return $this
     */
    public function setListArray($value)
    {
        $this->list_array = $value;
        return $this;
    }
}