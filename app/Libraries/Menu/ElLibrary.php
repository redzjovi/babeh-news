<?php

namespace App\Libraries\Menu;

use Illuminate\Support\Str;

class ElLibrary
{
    private $id;
    private $list = [];
    private $reference_id;
    private $reference_title;
    private $reference_type;
    private $title;
    private $url;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        if (isset($attributes['id'])) {
            $this->setId($attributes['id']);
        }
        if (isset($attributes['list'])) {
            $this->setList($attributes['list']);
        }
        if (isset($attributes['reference_id'])) {
            $this->setReferenceId($attributes['reference_id']);
        }
        if (isset($attributes['reference_title'])) {
            $this->setReferenceTitle($attributes['reference_title']);
        }
        if (isset($attributes['reference_type'])) {
            $this->setReferenceType($attributes['reference_type']);
        }
        if (isset($attributes['title'])) {
            $this->setTitle($attributes['title']);
        }
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return [
            'id' => $this->getId(),
            'list' => $this->getList(),
            'reference_id' => $this->getReferenceId(),
            'reference_title' =>  $this->getReferenceTitle(),
            'reference_type' =>  $this->getReferenceType(),
            'title' =>  $this->getTitle(),
            'url' => $this->getUrl(),
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getList()
    {
        if (count($this->list) > 0) {
            foreach ($this->list as $i => $el) {
                $el = new ElLibrary($el);
                $this->list[$i] = $el->getAttributes();
            }
        }

        return $this->list;
    }

    /**
     * @return int
     */
    public function getReferenceId()
    {
        return $this->reference_id;
    }

    /**
     * @return string
     */
    public function getReferenceTitle()
    {
        return $this->reference_title;
    }

    /**
     * @return string
     */
    public function getReferenceType()
    {
        return $this->reference_type;
    }

    /**
     * @return string $title
     */
    public function getTitle()
    {
        $title = $this->reference_title;
        $title = $this->title ? $this->title : $title;
        return $title;
    }

    /**
     * @return string $url
     */
    public function getUrl()
    {
        $url = '';
        
        if ($this->reference_type == 'advertisement') {
            $url = route('advertisement.show', [$this->reference_id, 'slug' => Str::slug($this->reference_title)]);
        } elseif ($this->reference_type == 'category') {
            $url = route('post.category.show', [$this->reference_id, 'slug' => Str::slug($this->reference_title)]);
        } elseif ($this->reference_type == 'post') {
            $url = route('post.show', [$this->reference_id, 'slug' => Str::slug($this->reference_title)]);
        } elseif ($this->reference_type == 'tag') {
            $url = route('post.tag.show', [$this->reference_id, 'slug' => Str::slug($this->reference_title)]);
        }

        return $url;
    }

    /**
     * @param int $value
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * @param array $value
     */
    public function setList($value)
    {
        $this->list = $value;
    }

    /**
     * @param int $value
     */
    public function setReferenceId($value)
    {
        $this->reference_id = $value;
    }

    /**
     * @param string $value
     */
    public function setReferenceTitle($value)
    {
        $this->reference_title = $value;
    }

    /**
     * @param string $value
     */
    public function setReferenceType($value)
    {
        $this->reference_type = $value;
    }

    /**
     * @param string $value
     */
    public function setTitle($value)
    {
        $this->title = $value;
    }
}