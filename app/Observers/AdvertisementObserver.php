<?php

namespace App\Observers;

use App\Models\Advertisement;

class AdvertisementObserver
{
    /**
     * @param \App\Advertisement $advertisement
     * @return void
     */
    public function saving(Advertisement $advertisement)
    {
        $advertisement->setSlugAttribute($advertisement->title);
    }
}
