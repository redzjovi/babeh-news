<?php

namespace App\Observers;

use App\Models\Tag;

class TagObserver
{
    /**
     * @param \App\Tag $tag
     * @return void
     */
    public function saving(Tag $tag)
    {
        $tag->setSlugAttribute($tag->name);
    }
}
