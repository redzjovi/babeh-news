<?php

namespace App\Observers;

use App\Models\Post;

class PostObserver
{
    /**
     * @param \App\Post $post
     * @return void
     */
    public function saving(Post $post)
    {
        $post->setSlugAttribute($post->title);
    }
}
