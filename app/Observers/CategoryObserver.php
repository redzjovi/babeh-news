<?php

namespace App\Observers;

use App\Models\Category;

class CategoryObserver
{
    /**
     * @param \App\Category $category
     * @return void
     */
    public function saving(Category $category)
    {
        $category->setSlugAttribute($category->name);
    }
}
