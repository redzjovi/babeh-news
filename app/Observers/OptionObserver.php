<?php

namespace App\Observers;

use App\Models\Option;
use Cache;

class OptionObserver
{
    /**
     * @param Option $option
     * @return void
     */
    public function deleted(Option $option)
    {
        Cache::forget('option:name:'.$option->name);
    }

    /**
     * @param Option $option
     * @return void
     */
    public function saved(Option $option)
    {
        Cache::forget('option:name:'.$option->name);
    }
}
