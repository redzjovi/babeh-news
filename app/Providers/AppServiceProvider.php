<?php

namespace App\Providers;

use App\Models\{
    Advertisement,
    Category,
    Option,
    Post,
    Tag
};
use App\Observers\{
    AdvertisementObserver,
    CategoryObserver,
    OptionObserver,
    PostObserver,
    TagObserver
};
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('app.env') == 'production') {
            URL::forceScheme('https');
        }
        
        Advertisement::observe(AdvertisementObserver::class);
        Category::observe(CategoryObserver::class);
        Option::observe(OptionObserver::class);
        Post::observe(PostObserver::class);
        Schema::defaultStringLength(191);
        Tag::observe(TagObserver::class);
    }
}
