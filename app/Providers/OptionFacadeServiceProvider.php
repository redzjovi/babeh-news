<?php

namespace App\Providers;

use App\Repositories\OptionRepository;
use Illuminate\Support\ServiceProvider;

class OptionFacadeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('option_facade', OptionRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
