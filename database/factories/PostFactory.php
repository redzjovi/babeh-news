<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Post::class, function (Faker $faker) {
    $title = $faker->text(70);

    return [
        'title' => $title,
        'slug' => Str::slug($title),
        'excerpt' => $faker->text(),
        'content' => $faker->paragraph(100),
    ];
});
