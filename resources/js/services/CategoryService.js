export default new class {
  getCategoriesWhereIdIn(ids) {
    return axios.get('/api/category', {
      params: {
        'ids': ids
      }
    }).then(res => {
      return res.data;
    });
  }
  getCategoriesWhereNameOrderByName(name) {
    return axios.get('/api/category', {
      params: {
        'name': name,
        'order_by': 'name'
      }
    }).then(res => {
      return res.data;
    });
  }
  getAdvertisementWhereId(id) {
    return axios.get('/api/category/' + id).then(res => {
      return res.data;
    });
  }
}
