export default new class {
  getMenusWhereNameOrderByName(name) {
    return axios.get('/api/menu', {
      params: {
        'name': name,
        'order_by': 'name'
      }
    }).then(res => {
      return res.data;
    });
  }
  getMenuWhereId(id) {
    return axios.get('/api/menu/' + id).then(res => {
      return res.data;
    });
  }
}
