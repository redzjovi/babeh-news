export default new class {
  getPostsWhereIdIn(ids) {
    return axios.get('/api/post', {
      params: {
        'ids': ids
      }
    }).then(res => {
      return res.data;
    });
  }
  getPostsWhereTitleOrderByTitle(title) {
    return axios.get('/api/post', {
      params: {
        'title': title,
        'order_by': 'title'
      }
    }).then(res => {
      return res.data;
    });
  }
  getPostWhereId(id) {
    return axios.get('/api/post/' + id).then(res => {
      return res.data;
    });
  }
}
