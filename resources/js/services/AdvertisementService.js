export default new class {
  getAdvertisementsWhereIdIn(ids) {
    return axios.get('/api/advertisement', {
      params: {
        'ids': ids
      }
    }).then(res => {
      return res.data;
    });
  }
  getAdvertisementsWhereTitleOrderByTitle(title) {
    return axios.get('/api/advertisement', {
      params: {
        'title': title,
        'order_by': 'title'
      }
    }).then(res => {
      return res.data;
    });
  }
  getAdvertisementWhereId(id) {
    return axios.get('/api/advertisement/' + id).then(res => {
      return res.data;
    });
  }
}
