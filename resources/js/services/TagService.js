export default new class {
  getTagsWhereNameOrderByName(name) {
    return axios.get('/api/tag', {
      params: {
        'name': name,
        'order_by': 'name'
      }
    }).then(res => {
      return res.data;
    });
  }
  getTagWhereId(id) {
    return axios.get('/api/tag/' + id).then(res => {
      return res.data;
    });
  }
}
