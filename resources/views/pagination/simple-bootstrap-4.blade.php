@if ($paginator->hasPages())
    <div class="d-flex justify-content-between">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <div></div>
        @else
            <div>
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a>
            </div>
        @endif
        
        <div></div>
        
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <div>
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a>
            </div>
        @else
            <div></div>
        @endif
    </div>
@endif
