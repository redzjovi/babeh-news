@extends('layout/frontend')

@section('body_content')
    <div class="container">
        <div class="justify-content-between row">
            @if ($posts->count() > 0)
                <div class="col-md-8">
                    <h5 class="font-weight-bold spanborder">
                        <span>All Stories</span>
                    </h5>
                    @foreach ($posts as $post)
                        <anchor-with-ads-link ads-link-prop="{{ optional($post->postAds)->link }}">
                            <div class="mb-3 d-flex justify-content-between">
                                <div class="pr-3">
                                    <h2 class="font-weight-bold h4 mb-1">
                                        <a class="text-dark" href="{{ route('post.show', [$post->id, 'slug' => $post->slug]) }}">{{ $post->title }}</a>
                                    </h2>
                                    <p>
                                        {{ \Illuminate\Support\Str::limit($post->excerpt, 300, '...') }}
                                    </p>
                                    <small class="text-muted">
                                        {{ $post->created_at->format('d F Y H:i:s') }}
                                    </small>
                                </div>
                                @if ($medium = $post->getImage())
                                    <img src="{{ $medium->getFilePathUrlAttribute() }}" style="object-fit: contain" width="120" />
                                @else
                                    <img src="https://via.placeholder.com/120x120" style="object-fit: contain" width="120" />
                                @endif
                            </div>
                        </anchor-with-ads-link>
                    @endforeach
                    {{ $posts->links('pagination/simple-bootstrap-4') }}
                </div>
            @endif

            @if ($prediction)
                <div class="col-md-4 d-none d-md-block pl-4">
                    <div class="sticky-top">
                        <h5 class="font-weight-bold spanborder">
                            <span>{{ $prediction->title }}</span>
                        </h5>
                        <div>{!! $prediction->content !!}</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
