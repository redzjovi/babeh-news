@extends('layout/frontend')

@section('head_meta_description', $post->excerpt)

@section('head_meta_keywords', $post->tags()->pluck('name')->join(', '))

@section('head_title', $post->title)

@section('body_content')
    <div class="container">
        <div class="bg-white jumbotron jumbotron-fluid mb-3 mt-3 pb-0 pl-0 position-relative pt-0">
            <div class="h-100 tofront">
                <div class="align-self-center">
                    <h1 class="display-4 font-weight-bold mb-3 secondfont">{{ $post->title }}</h1>
                    <div class="align-items-center d-flex">
                        <small class="ml-2">
                            <span class="d-block text-muted">{{ $post->created_at->format('d F Y H:i:s') }}</span>
                        </small>
                    </div>
                    <div align="center">
                        @if ($medium = $post->getImage())
                            <img class="img-fluid" src="{{ $medium->getFilePathUrlAttribute() }}" />
                        @else
                            <img class="img-fluid" src="https://via.placeholder.com/800x400" />
                        @endif
                    </div>
                    <p class="mb-3">{{ $post->excerpt }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header -->

    <div class="container pb-4 pt-4">
        <div class="justify-content-center row">
            <div class="col-lg-2 col-md-12 mb-4 pr-4">
                <div class="sticky-top text-center">
                    <div class="text-muted">
                        Share this
                    </div>
                    <div class="d-inline-block share">
                        <!-- AddToAny BEGIN -->
                        <div class="a2a_default_style a2a_kit a2a_kit_size_32">
                            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                        </div>
                        <!-- AddToAny END -->
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12">
                <article class="article-post">
                    {!! $post->content !!}
                </article>
                @if ($post->categories->count() > 0)
                    <div class="card">
                        <div class="card-body">
                            Category :
                            @foreach ($post->categories->sortBy('name') as $category)
                                <a class="badge badge-primary" href="{{ route('post.category.show', [$category->id, 'slug' => $category->slug]) }}">
                                    {{ $category->name }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endif
                @if ($post->tags->count() > 0)
                    <div class="card">
                        <div class="card-body">
                            Tag :
                            @foreach ($post->tags as $tag)
                                <a class="badge badge-primary" href="{{ route('post.tag.show', [$tag->id, 'slug' => $tag->slug]) }}">
                                    {{ $tag->name }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @if ($relatedPosts->count() > 0)
        <div class="container pb-4 pt-4">
            <h5 class="font-weight-bold spanborder">
                <span>Read next</span>
            </h5>
            <div class="row">
                @foreach ($relatedPosts as $post)
                    @if ($loop->first)
                        <div class="col-lg-6">
                            <anchor-with-ads-link ads-link-prop="{{ optional($post->postAds)->link }}">
                                <div class="border-0 box-shadow card h-xl-300 mb-4">
                                    @if ($medium = $post->getImage())
                                        <div style="background-image: url('{{ $medium->getFilePathUrlAttribute() }}'); background-repeat: no-repeat; background-size: cover; height: 150px;"></div>
                                    @else
                                        <div style="background-image: url('https://via.placeholder.com/300x150'); background-repeat: no-repeat; background-size: cover; height: 150px;"></div>
                                    @endif
                                    <div class="align-items-start card-body d-flex flex-column pb-0 px-0">
                                        <h2 class="font-weight-bold h4">
                                            <a class="text-dark" href="{{ route('post.show', [$post->id, 'slug' => $post->slug]) }}">{{ $post->title }}</a>
                                        </h2>
                                        <p class="card-text">
                                            {{ \Illuminate\Support\Str::limit($post->excerpt, 300, '...') }}
                                        </p>
                                        <div>
                                            <small class="text-muted">
                                                {{ $post->created_at->format('d F Y H:i:s') }}
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </anchor-with-ads-link>
                        </div>
                    @endif
                @endforeach

                <div class="col-lg-6">
                    <div class="box-shadow flex-md-row h-xl-300 mb-4">
                        @foreach ($relatedPosts as $post)
                            @if (! $loop->first)
                                <anchor-with-ads-link ads-link-prop="{{ optional($post->postAds)->link }}">
                                    <div class="align-items-center d-flex mb-3">
                                        @if ($medium = $post->getImage())
                                            <img src="{{ $medium->getFilePathUrlAttribute() }}" width="160" />
                                        @else
                                            <img src="https://via.placeholder.com/160x160" width="160" />
                                        @endif
                                        <div class="pl-3">
                                            <h2 class="font-weight-bold h6 mb-2">
                                                <a class="text-dark" href="{{ route('post.show', [$post->id, 'slug' => $post->slug]) }}">{{ $post->title }}</a>
                                            </h2>
                                            <div class="card-text small text-muted">
                                                {{ \Illuminate\Support\Str::limit($post->excerpt, 300, '...') }}
                                            </div>
                                            <small class="text-muted">
                                                {{ $post->created_at->format('d F Y H:i:s') }}
                                            </small>
                                        </div>
                                    </div>
                                </anchor-with-ads-link>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('body_script')
    <script async src="https://static.addtoany.com/menu/page.js"></script>                    
@endsection