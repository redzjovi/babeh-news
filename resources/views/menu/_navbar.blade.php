@if ($menuNavbar)
    <ul class="align-items-center d-flex mr-auto navbar-nav">
        @foreach ($menuNavbar as $menu)
            @if (count($menu['list']) > 0)
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                        {{ $menu['title'] }}
                    </a>
                    <div class="dropdown-menu">
                        @foreach ($menu['list'] as $menuChild)
                            <a class="dropdown-item" href="{{ $menuChild['url'] }}">
                                {{ $menuChild['title'] }}
                            </a>
                        @endforeach
                    </div>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{ $menu['url'] }}">
                        {{ $menu['title'] }}
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
@endif

