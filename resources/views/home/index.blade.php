@extends('layout/frontend')

@section('body_content')
    {{-- @if ($headerPost)
        <div class="container">
            <anchor-with-ads-link ads-link-prop="{{ optional($headerPost->postAds)->link }}">
                <div class="bg-lightblue jumbotron jumbotron-fluid mb-3 pb-0 position-relative pt-0">
                    <div class="h-100 pl-4 pr-0 tofront">
                        <div class="justify-content-between row">
                            <div class="align-self-center col-md-6 pb-6 pt-6">
                                <h1 class="font-weight-bold mb-3 secondfont">
                                    <a class="text-dark" href="{{ route('post.show', [$headerPost->id, 'slug' => $headerPost->slug]) }}">{{ $headerPost->title }}</a>
                                </h1>
                                <p class="mb-3">{{ $headerPost->excerpt }}</p>
                                <a class="btn btn-dark" href="{{ route('post.show', [$headerPost->id, 'slug' => $headerPost->slug]) }}">Read More</a>
                            </div>
                            <div class="col-md-6 d-md-block d-none pr-0" style="background-image:url(./template-mundana-bootstrap-html-master/assets/img/demo/home.jpg); background-size:cover;"></div>
                        </div>
                    </div>
                </div>
            </anchor-with-ads-link>
        </div>
    @endif --}}

    @if ($headerPosts)
        <div class="container">
            <div class="carousel mb-3 slide" data-ride="carousel" id="carouselExampleIndicators">
                <ol class="carousel-indicators">
                    @foreach ($headerPosts as $i => $headerPost)
                        @if ($post = $headerPost->post)
                            <li class="{{ $loop->first ? 'active' : '' }}" data-slide-to="{{ $i }}" data-target="#carouselExampleIndicators"></li>
                        @endif
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach ($headerPosts as $headerPost)
                        @if ($post = $headerPost->post)
                            <div class="{{ $loop->first ? 'active' : '' }} carousel-item">
                                @if ($medium = $post->getImage())
                                    <img alt="{{ $post->title }}" class="d-block w-100" src="{{ $medium->getFilePathUrlAttribute() }}" />
                                @else
                                    <img alt="{{ $post->title }}" class="d-block w-100" src="https://via.placeholder.com/800x400" />
                                @endif
                                <div class="carousel-caption d-md-block d-none">
                                    <a href="{{ route('post.show', [$post->id, 'slug' => $post->slug]) }}">
                                        <h5>
                                            {{ $post->title }}
                                        </h5>
                                    </a>
                                    <p>...</p>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    @endif

    <div class="container">
        <div class="justify-content-between row">
            @if ($storyPosts->count() > 0)
                <div class="col-md-8">
                    <h5 class="font-weight-bold spanborder">
                        <span>All Stories</span>
                    </h5>
                    @foreach ($storyPosts as $post)
                        <anchor-with-ads-link ads-link-prop="{{ optional($post->postAds)->link }}">
                            <div class="mb-3 d-flex justify-content-between">
                                <div class="pr-3">
                                    <h2 class="font-weight-bold h4 mb-1">
                                        <a class="text-dark" href="{{ route('post.show', [$post->id, 'slug' => $post->slug]) }}">{{ $post->title }}</a>
                                    </h2>
                                    <p>
                                        {{ \Illuminate\Support\Str::limit($post->excerpt, 300, '...') }}
                                    </p>
                                    <small class="text-muted">
                                        {{ $post->created_at->format('d F Y H:i:s') }}
                                    </small>
                                </div>
                                @if ($medium = $post->getImage())
                                    <img src="{{ $medium->getFilePathUrlAttribute() }}" style="object-fit: contain" width="120" />
                                @else
                                    <img src="https://via.placeholder.com/120x120" style="object-fit: contain" width="120" />
                                @endif
                            </div>
                        </anchor-with-ads-link>

                        @if ($loop->iteration == 5 && $middleAdvertisements->count() > 0)
                            <div class="carousel mb-3 slide" data-ride="carousel" id="middleAdvertisementsCarousel">
                                <ol class="carousel-indicators">
                                    @foreach ($middleAdvertisements as $i => $middleAdvertisement)
                                        @if ($advertisement = $middleAdvertisement->advertisement)
                                            <li class="{{ $loop->first ? 'active' : '' }}" data-slide-to="{{ $i }}" data-target="#middleAdvertisementsCarousel"></li>
                                        @endif
                                    @endforeach
                                </ol>
                                <div class="carousel-inner">
                                    @foreach ($middleAdvertisements as $middleAdvertisement)
                                        @if ($advertisement = $middleAdvertisement->advertisement)
                                            <div class="{{ $loop->first ? 'active' : '' }} carousel-item">
                                                @if ($medium = $advertisement->getImage())
                                                    <img alt="{{ $advertisement->title }}" class="d-block w-100" src="{{ $medium->getFilePathUrlAttribute() }}" />
                                                @else
                                                    <img alt="{{ $advertisement->title }}" class="d-block w-100" src="https://via.placeholder.com/400x200" />
                                                @endif
                                                <div class="carousel-caption d-md-block d-none">
                                                    <a href="{{ route('advertisement.show', [$advertisement->id, 'slug' => $advertisement->slug]) }}">
                                                        <h5>
                                                            {{ $advertisement->title }}
                                                        </h5>
                                                    </a>
                                                    <p>...</p>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#middleAdvertisementsCarousel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#middleAdvertisementsCarousel" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @endif
                        
                        @if ($loop->iteration == 10 && $bottomAdvertisements->count() > 0)
                            <div class="carousel mb-3 slide" data-ride="carousel" id="bottomAdvertisementsCarousel">
                                <ol class="carousel-indicators">
                                    @foreach ($bottomAdvertisements as $i => $bottomAdvertisement)
                                        @if ($advertisement = $bottomAdvertisement->advertisement)
                                            <li class="{{ $loop->first ? 'active' : '' }}" data-slide-to="{{ $i }}" data-target="#bottomAdvertisementsCarousel"></li>
                                        @endif
                                    @endforeach
                                </ol>
                                <div class="carousel-inner">
                                    @foreach ($bottomAdvertisements as $bottomAdvertisement)
                                        @if ($advertisement = $bottomAdvertisement->advertisement)
                                            <div class="{{ $loop->first ? 'active' : '' }} carousel-item">
                                                @if ($medium = $advertisement->getImage())
                                                    <img alt="{{ $advertisement->title }}" class="d-block w-100" src="{{ $medium->getFilePathUrlAttribute() }}" />
                                                @else
                                                    <img alt="{{ $advertisement->title }}" class="d-block w-100" src="https://via.placeholder.com/400x200" />
                                                @endif
                                                <div class="carousel-caption d-md-block d-none">
                                                    <a href="{{ route('advertisement.show', [$advertisement->id, 'slug' => $advertisement->slug]) }}">
                                                        <h5>
                                                            {{ $advertisement->title }}
                                                        </h5>
                                                    </a>
                                                    <p>...</p>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#bottomAdvertisementsCarousel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#bottomAdvertisementsCarousel" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @endif
                        
                        @if ($loop->iteration % 5 == 0 && $prediction)
                            <div class="mb-3 d-flex d-block d-sm-none justify-content-between">
                                <div class="mb-3 d-flex justify-content-between">
                                    <div class="pr-3">
                                        <h2 class="font-weight-bold h4 mb-1">{{ $prediction->title }}</h2>
                                        <p>{!! $prediction->content !!}</p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif

            @if ($prediction)
                <div class="col-md-4 d-none d-md-block pl-4">
                    <div class="sticky-top">
                        <h5 class="font-weight-bold spanborder">
                            <span>{{ $prediction->title }}</span>
                        </h5>
                        <div>{!! $prediction->content !!}</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
