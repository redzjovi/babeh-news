@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show">
        <button aria-label="Close" class="close" data-dismiss="alert" type="button">
            <span aria-hidden="true">&times;</span>
         </button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@foreach (['danger', 'info', 'success', 'warning'] as $type)
    @if (session()->has('alert-'.$type))
        <div class="alert alert-{{ $type }} alert-dismissible fade show">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button">
                <span aria-hidden="true">&times;</span>
             </button>
            {{ session()->get('alert-' . $type) }}
        </div>
    @endif
@endforeach
