@extends('layout/frontend')

@section('head_meta_description', $advertisement->excerpt)

@section('head_meta_keywords', $advertisement->tags()->pluck('name')->join(', '))

@section('head_title', $advertisement->title)

@section('body_content')
    <div class="container">
        <div class="bg-white jumbotron jumbotron-fluid mb-3 mt-3 pb-0 pl-0 position-relative pt-0">
            <div class="h-100 tofront">
                <div class="align-self-center">
                    <h1 class="display-4 font-weight-bold mb-3 secondfont">{{ $advertisement->title }}</h1>
                    <div class="align-items-center d-flex">
                        <small class="ml-2">
                            <span class="d-block text-muted">{{ $advertisement->created_at->format('d F Y H:i:s') }}</span>
                        </small>
                    </div>
                    <div align="center">
                        @if ($medium = $advertisement->getImage())
                            <img class="img-fluid" src="{{ $medium->getFilePathUrlAttribute() }}" />
                        @else
                            <img class="img-fluid" src="https://via.placeholder.com/800x400" />
                        @endif
                    </div>
                    <p class="mb-3">{{ $advertisement->excerpt }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header -->

    <div class="container pb-4 pt-4">
        <div class="justify-content-center row">
            <div class="col-lg-2 col-md-12 mb-4 pr-4">
                <div class="sticky-top text-center">
                    <div class="text-muted">
                        Share this
                    </div>
                    <div class="d-inline-block share">
                        <!-- AddToAny BEGIN -->
                        <div class="a2a_default_style a2a_kit a2a_kit_size_32">
                            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                        </div>
                        <!-- AddToAny END -->
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12">
                <article class="article-post">
                    {!! $advertisement->content !!}
                </article>
            </div>
        </div>
    </div>
@endsection

@section('body_script')
    <script async src="https://static.addtoany.com/menu/page.js"></script>                    
@endsection