<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @yield('head')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <link href="{{ asset('AdminLTE-3.0.1/plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('template-mundana-bootstrap-html-master/assets/css/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('template-mundana-bootstrap-html-master/assets/img/favicon.ico') }}" rel="apple-touch-icon" sizes="76x76" />
    <link href="{{ asset('template-mundana-bootstrap-html-master/assets/img/favicon.ico') }}" rel="icon" type="image/png" />
    @yield('body_link')
    <meta charset="utf-8" />
    <meta content="{{ csrf_token() }}" name="csrf-token" />
    <meta content="@yield('head_meta_description')" name="description" />
    <meta content="@yield('head_meta_keywords')" name="keywords" />
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
    <meta content="initial-scale=1.0, maximum-scale=1.0, shrink-to-fit=no, user-scalable=0, width=device-width" name="viewport" />
    <title>@yield('head_title', OptionFacade::get('html__head__title'))</title>
</head>
<body>
    <div id="app">
        <!--------------------------------------
        NAVBAR
        --------------------------------------->
        <nav class="bg-white fixed-top navbar navbar-expand-lg navbar-light topnav">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home.index') }}">
                    @if (OptionFacade::get('body__navbar_brand__type') == 'image')
                        @if ($site__image__src = OptionFacade::get('site__image_src'))
                            <img height="50" src="{{ $site__image__src }}" />
                        @else
                            <img height="50" src="https://via.placeholder.com/200x200" />
                        @endif
                    @elseif (OptionFacade::get('body__navbar_brand__type') == 'text')
                        <strong>{{ OptionFacade::get('site__name') }}</strong>
                    @endif
                </a>
                <button aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation" class="collapsed navbar-toggler" data-target="#navbarColor02" data-toggle="collapse" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarColor02">
                    @include('menu/_navbar')
                    <ul class="align-items-center d-flex ml-auto navbar-nav">
                        @guest
                            <li class="nav-item d-none">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="dropdown nav-item">
                                <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle nav-link" data-toggle="dropdown" id="navbarDropdown" href="#" role="button" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div aria-labelledby="navbarDropdown" class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->

        @yield('body_content')

        <!--------------------------------------
        FOOTER
        --------------------------------------->
        <div class="container mt-5">
            <footer class="bg-white border-top p-3 small text-muted">
                <div class="align-items-center justify-content-between row">
                    <div>
                        Copyright &copy; {{ date('Y') }}.
                        All rights reserved.
                    </div>
                    <div>
                        Made with &hearts;.
                    </div>
                </div>
            </footer>
        </div>
        <!-- End Footer -->
    </div>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('template-mundana-bootstrap-html-master/assets/js/vendor/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template-mundana-bootstrap-html-master/assets/js/functions.js') }}" type="text/javascript"></script>
    @yield('body_script')
</body>
</html>
