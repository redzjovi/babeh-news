<!DOCTYPE html>
<html>
<head>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <link href="{{ asset('AdminLTE-3.0.1/dist/css/adminlte.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('AdminLTE-3.0.1/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet" />
    <link href="{{ asset('AdminLTE-3.0.1/plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="initial-scale=1, width=device-width" name="viewport" />
    <title>@yield('head_title')</title>
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper" id="app">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="elevation-4 main-sidebar sidebar-dark-primary">
            <!-- Brand Logo -->
            <a class="brand-link" href="{{ route('backend.index') }}">
                <img alt="AdminLTE Logo" class="brand-image elevation-3 img-circle" src="{{ asset('AdminLTE-3.0.1/dist/img/AdminLTELogo.png') }}" style="opacity: .8" />
                <span class="brand-text font-weight-light">{{ config('app.name') }}</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="d-flex mb-3 mt-3 pb-3 user-panel">
                    <div class="image">
                        <img alt="User Image" class="elevation-2 img-circle" src="{{ asset('AdminLTE-3.0.1/dist/img/user2-160x160.jpg') }}" />
                    </div>
                    <div class="info">
                        <a class="d-block" href="#">{{ auth()->user()->name }}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="user-panel">
                    <ul class="flex-column nav nav-pills nav-sidebar" data-accordion="false" data-widget="treeview" role="menu">
                        <li class="has-treeview nav-item">
                            <a href="#" class="nav-link">
                                <i class="fas fa-home nav-icon"></i>
                                <p>
                                    Home
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('backend.home-header-post.index') }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Home Header Post</p>
                                    </a>
                                </li>
                                <li class="d-none nav-item">
                                    <a class="nav-link" href="{{ route('backend.home-main-post.index') }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Home Main Post</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('backend.home-middle-advertisement.index') }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Home Middle Advertisement</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('backend.home-bottom-advertisement.index') }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Home Bottom Advertisement</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('backend.advertisement.index') }}">
                                <i class="nav-icon fas fa-file"></i>
                                <p>Advertisement</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('backend.category.index') }}">
                                <i class="nav-icon fas fa-tags"></i>
                                <p>Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('backend.menu.index') }}">
                                <i class="nav-icon fas fa-bars"></i>
                                <p>Menu</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('backend.post.index') }}">
                                <i class="nav-icon fas fa-file"></i>
                                <p>Post</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('backend.prediction.index') }}">
                                <i class="nav-icon fas fa-file"></i>
                                <p>Prediction</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('backend.setting.index') }}">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>Setting</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('backend.tag.index') }}">
                                <i class="nav-icon fas fa-tags"></i>
                                <p>Tag</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt nav-icon"></i>
                                {{ __('Logout') }}
                            </a>

                            <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    @include('partial/_alert')
                    @yield('body_content_header')
                </div>
            </section>
            <section class="content">
                <div class="container-fluid">
                    @yield('body_content')
                </div>
            </section>
        </div>
    </div>
    <!-- ./wrapper -->

    <script src="{{ asset('js/app.js') }}"></script>

    <script src="{{ asset('AdminLTE-3.0.1/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.1/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('AdminLTE-3.0.1/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    @yield('script')
</body>
</html>
