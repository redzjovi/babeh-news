@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('backend.home-middle-advertisement.index') }}">Home Middle Advertisement</a>
                </li>
                <li class="active breadcrumb-item">Edit</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('backend.home-middle-advertisement.update') }}" class="form-horizontal" method="post">
                @csrf
                @method('PUT')
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-form-label col-sm-2">Advertisement(s) *</label>
                            <div class="col-sm-10">
                                <advertisement-multiple-select
                                    name-prop="advertisement_id[]"
                                    style-prop="width: 100%"
                                    :values-prop="{{ json_encode($homeMiddleAdvertisement->pluck('advertisement')->all()) }}"
                                >
                                </advertisement-multiple-select>
                            </div>
                        </div>
                    </div><!-- /.card-body -->
                    <div class="card-footer">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div><!-- /.card-footer -->
                </div><!-- /.card -->
            </form>
        </div>
    </div>
@endsection
