@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="active breadcrumb-item">Setting</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('backend.setting.update') }}" class="form-horizontal" method="post">
                @csrf
                @method('PUT')
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-4">Body - Navbar Brand - Type</label>
                                    <div class="col-sm-8">
                                        <div class="form-check">
                                            <input {{ old('body__navbar_brand__type', $body__navbar_brand__type->value) == 'image' ? 'checked' : '' }} class="form-check-input" id="body__navbar_brand__type__image" name="body__navbar_brand__type" type="radio" value="image" />
                                            <label class="form-check-label" for="body__navbar_brand__type__image">Image</label>
                                        </div>
                                        <div class="form-check">
                                            <input {{ old('body__navbar_brand__type', $body__navbar_brand__type->value) == 'text' ? 'checked' : '' }} class="form-check-input" id="body__navbar_brand__type__text" name="body__navbar_brand__type" type="radio" value="text" />
                                            <label class="form-check-label" for="body__navbar_brand__type__text">Text</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-4">Body - Navbar - Menu - Id</label>
                                    <div class="col-sm-8">
                                        <element-ui-menu-select name-prop="body__navbar__menu_id" style-prop="width: 100%" :value-id-prop="{{ $body__navbar__menu_id->value ? $body__navbar__menu_id->value : 0 }}"></element-ui-menu-select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-4">Html - Head - Title</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="html__head__title" type="text" value="{{ old('html__head__title', $html__head__title->value) }}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-4">Site - Image Src</label>
                                    <div class="col-sm-8">
                                        <element-ui-medium-upload-image
                                            accept-prop="image/*"
                                            name-prop="site__image_id[]"
                                            :file-list-prop="{{ $site__image_id->medium ? json_encode($site__image_id->medium->getImagesToElementUiUploadFileList()) : '[]' }}"
                                            :limit-prop="1"
                                        >
                                        </element-ui-medium-upload-image>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-4">Site - Name</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="site__name" type="text" value="{{ old('site__name', $site__name->value) }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.card-body -->
                    <div class="card-footer">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div><!-- /.card-footer -->
                </div><!-- /.card -->
            </form>
        </div>
    </div>
@endsection
