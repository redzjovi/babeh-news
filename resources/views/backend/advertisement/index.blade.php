@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="active breadcrumb-item">Advertisement</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('backend.advertisement.create') }}">
                        <i class="fas fa-plus"></i>
                    </a>
                </div>
                <div class="card-body">
                    <table class="datatable table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Slug</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($advertisements as $advertisement)
                                <tr>
                                    <td>{{ $advertisement->title }}</td>
                                    <td>{{ $advertisement->slug }}</td>
                                    <td>
                                        <a href="{{ route('backend.advertisement.edit', $advertisement->id) }}">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('backend.advertisement.delete', $advertisement->id) }}">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div><!-- /.card -->
        </div>
    </div>
@endsection

@section('script')
    <script>
    $('.datatable').DataTable({
        'order': []
    });
    </script>
@endsection
