@csrf
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-form-label">Title *</label>
                    <input class="form-control" name="title" required type="text" value="{{ old('title', $model->title) }}" />
                    @error('title')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label">Slug</label>
                    <input class="form-control" disabled type="text" value="{{ $model->slug }}" />
                </div>
                <div class="form-group">
                    <label class="col-form-label">Excerpt</label>
                    <textarea class="form-control" name="excerpt" rows="5">{{ old('excerpt', $model->excerpt) }}</textarea>
                </div>            
                <div class="form-group">
                    <label class="col-form-label">Content</label>
                    <textarea-tinymce
                        name-prop="content"
                        value-prop="{{ old('content', $model->content) }}"
                    >
                    </textarea-tinymce>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">Tag</label>
                    <div class="card">
                        <div class="card-body">
                            <element-ui-tag-tag name-prop="tag[name][]" :value-prop="{{ json_encode($model->tags()->pluck('name')->all()) }}"></element-ui-tag>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Image</label>
                    <div class="card">
                        <div class="card-body">
                            <element-ui-medium-upload-image
                                accept-prop="image/*"
                                name-prop="image[]"
                                :file-list-prop="{{ $model->getImagesToElementUiUploadFileList() }}"
                                :multiple-prop="true"
                            >
                            </element-ui-medium-upload-image>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.card-body -->
    <div class="card-footer">
        <button class="btn btn-success" type="submit">Save</button>
    </div><!-- /.card-footer -->
</div><!-- /.card -->
