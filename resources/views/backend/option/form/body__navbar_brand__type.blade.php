@csrf
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-form-label">Name *</label>
                    <input class="form-control" name="name" required type="text" value="{{ old('name', $model->name) }}" />
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label">Value</label>
                    <div class="form-check">
                        <input {{ old('value', $model->value) == 'image' ? 'checked' : '' }} class="form-check-input" id="value__image" name="value" type="radio" value="image" />
                        <label class="form-check-label" for="value__image">Image</label>
                    </div>
                    <div class="form-check">
                        <input {{ old('value', $model->value) == 'text' ? 'checked' : '' }} class="form-check-input" id="value__text" name="value" type="radio" value="text" />
                        <label class="form-check-label" for="value__text">Text</label>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div><!-- /.card-body -->
    <div class="card-footer">
        <button class="btn btn-success" type="submit">Save</button>
    </div><!-- /.card-footer -->
</div><!-- /.card -->
