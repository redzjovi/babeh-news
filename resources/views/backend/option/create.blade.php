@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('backend.option.index') }}">Option</a>
                </li>
                <li class="active breadcrumb-item">Create</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('backend.option.store') }}" class="form-horizontal" method="post">
                @include('backend/option/_form')
            </form>
        </div>
    </div>
@endsection
