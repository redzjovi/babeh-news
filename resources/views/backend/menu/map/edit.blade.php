@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('backend.menu.index') }}">Menu</a>
                </li>
                <li class="active breadcrumb-item">Map</li>
                <li class="active breadcrumb-item">Edit</li>
                <li class="active breadcrumb-item">{{ $model->id }}</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <backend-menu-map-edit-form 
        action="{{ route('backend.menu.map.update', $model->id) }}"
        delete-url="{{ route('backend.menu.map.delete', $model->id) }}"
        menu-name="{{ $model->name }}"
        method="POST"
        value-name-prop="value"
        :value-prop="{{ $model->value ? $model->value : '[]' }}"
        :menu-id="{{ $model->id }}"
    >
        @csrf
        @method('PUT')
        <input name="id" type="hidden" value="{{ $model->id }}" />
        <input name="name" type="hidden" value="{{ $model->name }}" />
    </backend-menu-map-edit-form>
@endsection
