@csrf
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-form-label">Name *</label>
                    <input class="form-control" name="name" required type="text" value="{{ old('name', $model->name) }}" />
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div><!-- /.card-body -->
    <div class="card-footer">
        <button class="btn btn-success" type="submit">Save</button>
    </div><!-- /.card-footer -->
</div><!-- /.card -->
