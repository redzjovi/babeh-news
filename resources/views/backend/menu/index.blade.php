@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="active breadcrumb-item">Menu</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('backend.menu.create') }}">
                        <i class="fas fa-plus"></i>
                    </a>
                </div>
                <div class="card-body">
                    <table class="datatable table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($menus as $menu)
                                <tr>
                                    <td>{{ $menu->name }}</td>
                                    <td>
                                        <a href="{{ route('backend.menu.edit', $menu->id) }}">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('backend.menu.delete', $menu->id) }}">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                        <a href="{{ route('backend.menu.map.edit', $menu->id) }}">
                                            <i class="fas fa-cog"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div><!-- /.card -->
        </div>
    </div>
@endsection

@section('script')
    <script>
    $('.datatable').DataTable({
        'order': []
    });
    </script>
@endsection
