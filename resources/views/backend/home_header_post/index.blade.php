@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('backend.home-header-post.index') }}">Home Header Post</a>
                </li>
                <li class="active breadcrumb-item">Edit</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('backend.home-header-post.update') }}" class="form-horizontal" method="post">
                @csrf
                @method('PUT')
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-form-label col-sm-2">Post(s) *</label>
                            <div class="col-sm-10">
                                <post-multiple-select
                                    name-prop="post_id[]"
                                    style-prop="width: 100%"
                                    :values-prop="{{ json_encode($homeHeaderPost->pluck('post')->all()) }}"
                                >
                                </post-multiple-select>
                            </div>
                        </div>
                    </div><!-- /.card-body -->
                    <div class="card-footer">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div><!-- /.card-footer -->
                </div><!-- /.card -->
            </form>
        </div>
    </div>
@endsection
