@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('backend.post.index') }}">Post</a>
                </li>
                <li class="active breadcrumb-item">Create</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('backend.post.store') }}" class="form-horizontal" method="post">
                @include('backend/post/_form')
            </form>
        </div>
    </div>
@endsection
