@csrf
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-form-label">Name *</label>
                    <input class="form-control" name="name" required type="text" value="{{ old('name', $model->name) }}" />
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label">Slug</label>
                    <input class="form-control" disabled type="text" value="{{ $model->slug }}" />
                </div>
                <div class="form-group">
                    <label class="col-form-label">Excerpt</label>
                    <textarea class="form-control" name="excerpt" rows="5">{{ old('excerpt', $model->excerpt) }}</textarea>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Content</label>
                    <textarea-tinymce
                        name-prop="content"
                        value-prop="{{ old('content', $model->content) }}"
                    >
                    </textarea-tinymce>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">Category</label>
                    <select class="form-control" name="parent_id">
                        <option value=""></option>
                        @foreach ($parents as $category)
                            @include('/backend/category/_form_category_option', [
                                'category' => $category,
                                'disabled' => false,
                                'model' => $model,
                                'tree_prefix' => '',
                            ])
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div><!-- /.card-body -->
    <div class="card-footer">
        <button class="btn btn-success" type="submit">Save</button>
    </div><!-- /.card-footer -->
</div><!-- /.card -->
