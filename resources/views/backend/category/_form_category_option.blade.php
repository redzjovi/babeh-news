@php
if ($category['id'] == $model->id) {
    $disabled = true;
}
@endphp

<option
    {{ $disabled ? 'disabled' : '' }}
    {{ $category['id'] == $model->parent_id ? 'selected' : '' }}
    value="{{ $category['id'] }}"
>
    {!! $tree_prefix !!}{{ $category['name'] }}
</option>
@if (isset($category['child']) && is_array($category['child']))
    @foreach ($category['child'] as $categoryChild)
        @include('/backend/category/_form_category_option', [
            'category' => $categoryChild,
            'disabled' => $disabled,
            'model' => $model,
            'tree_prefix' => $tree_prefix.'&nbsp;',
        ])
    @endforeach
@endif