@extends('layout/backend')

@section('body_content_header')
    <div class="row">
        <div class="col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('backend.category.index') }}">Category</a>
                </li>
                <li class="active breadcrumb-item">Edit</li>
                <li class="active breadcrumb-item">{{ $model->id }}</li>
            </ol>
        </div>
    </div>
@endsection

@section('body_content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('backend.category.update', $model->id) }}" class="form-horizontal" method="post">
                @method('PUT')
                <input name="id" type="hidden" value="{{ $model->id }}" />
                @include('backend/category/_form')
            </form>
        </div>
    </div>
@endsection
