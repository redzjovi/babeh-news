@csrf
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-form-label">Title *</label>
                    <input class="form-control" name="title" required type="text" value="{{ old('title', $model->title) }}" />
                    @error('title')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label">Content</label>
                    <textarea-tinymce
                        name-prop="content"
                        value-prop="{{ old('content', $model->content) }}"
                    >
                    </textarea-tinymce>
                </div>
            </div>
        </div>
    </div><!-- /.card-body -->
    <div class="card-footer">
        <button class="btn btn-success" type="submit">Save</button>
    </div><!-- /.card-footer -->
</div><!-- /.card -->
